package com.barq.abs.kafka.streams.template;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class InputSerde implements Serde<Input> {
    private InputSerializer serializer = new InputSerializer();
    private InputDeserializer deserializer = new InputDeserializer();


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<Input> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<Input> deserializer() {
        return deserializer;
    }

}
