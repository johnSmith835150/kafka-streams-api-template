package com.barq.abs.kafka.streams.template;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.io.Serializable;

public class Input implements Serializable {
//    define the variables to match the input schema
    public String someVariable;

    public static final Serializer<Input> InputSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<Input> InputDeserializer = new JsonPOJODeserializer<>(Input.class);


    public static Serde<Input> InputSerde;

    public Input() {
        // defualt constructor
    }

    public Input(String someVariable) {
        this.someVariable = someVariable;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
//        put all variables in a json object and cast it to string
        json.put("someVariable", someVariable);

        return json.toString();
    }
}
