package com.barq.abs.kafka.streams.template;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import static com.barq.abs.kafka.streams.template.StreamMain.serdeProps;

public class Output {
//    define the variables to match the input schema
    public char [] someOtherVariable;

    public static final Serializer<Output> outputSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<Output> outputDeserializer = new JsonPOJODeserializer<>(Output.class);


    public static Serde<Output> outputSerde;

    public Output() {
//        do nothing
    }

    public Output(String someOtherVariable) {
        this.someOtherVariable = someOtherVariable.toCharArray();
    }

    public static void createSerde() {
        if (outputSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", Output.class);
        outputSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", Output.class);
        outputDeserializer.configure(serdeProps, false);

        outputSerde = Serdes.serdeFrom(outputSerializer, outputDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
//        put all variables in a json object and cast it to string
        json.put("someOtherVariable", someOtherVariable);

        return json.toString();
    }
}
