package com.barq.abs.kafka.streams.template;

import com.google.gson.Gson;
import org.apache.kafka.common.serialization.Serializer;

import java.io.Closeable;
import java.nio.charset.Charset;
import java.util.Map;

public class InputSerializer implements Closeable, AutoCloseable, Serializer<Input> {
    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static Gson gson = new Gson();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String topic, Input data) {
        String line = gson.toJson(data);

        return line.getBytes(CHARSET);
    }

    @Override
    public void close() {

    }
}
