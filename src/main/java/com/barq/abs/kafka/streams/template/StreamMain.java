package com.barq.abs.kafka.streams.template;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;


public class StreamMain {
    private static String [] inputTopics = null;
    private static String outputTopic = null;
    public static Map<String, Object> serdeProps = new HashMap<>();

    public static void main(final String[] args) {
        ConfigLoader.loadConfig();
        Config config = Config.getInstance();

        inputTopics = config.inputTopics;
        outputTopic = config.outputTopic;

        final Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, config.applicationId);
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, config.clientId);
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, config.brokers);
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, InputSerde.class);
        properties.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        Output.createSerde();

        final StreamsBuilder builder = new StreamsBuilder();
        process(builder); // all processing happens here
        final KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private static void process(final StreamsBuilder builder) {
        final KStream<String, Input> source = builder.stream(Arrays.asList(inputTopics));

        KStream <String, Output> drain = source.
                map((k, v) -> {
                    Output output = new Output(v.someVariable);
                    return KeyValue.pair(k, output);
                });

        drain.foreach( (k, v) -> System.out.println(v) );
        drain.to(outputTopic, Produced.with(Serdes.String(), Output.outputSerde));
    }
}
