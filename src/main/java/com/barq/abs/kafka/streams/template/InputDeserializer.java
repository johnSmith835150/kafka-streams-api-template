package com.barq.abs.kafka.streams.template;

import com.google.gson.Gson;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.Closeable;
import java.util.Map;

public class InputDeserializer implements Closeable, AutoCloseable, Deserializer<Input> {
    static private Gson gson = new Gson();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public Input deserialize(String topic, byte[] bytes) {
        try {
            // Transform the bytes to String
            String person = new String(bytes);
            System.out.println(topic);
            System.out.println(person);
            // Return the Person object created from the String 'person'
            return gson.fromJson(person, Input.class);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error reading bytes", e);
        }
    }

    @Override
    public void close() {

    }
}
