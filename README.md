# **Kafka Streams API Template**
Template for Kafka Streams API processing application that maps an input class to an output class.

## Configure
Add your configuration in config.json in the resources directory for example
```json
{
	"inputTopics": ["input-topic-1", "input-topic-2", "input-topic-3"],
	"outputTopic": "output-topic",
	"brokers": "localhost:9092",
	"applicationId": "test-app",
	"clientId": "test-client"
}
```

## Build And Run
```sh
>> mvn clean package
>> mvn exec:java -Dexec.mainClass="com.barq.abs.kafka.streams.template.StreamMain"
```
